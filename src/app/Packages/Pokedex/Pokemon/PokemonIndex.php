<?php

namespace Pokedex\Packages\Pokedex\Pokemon;

use GuzzleHttp\Client;
use Pokedex\Contracts\PokemonIndexContract;
use Pokedex\Contracts\PokemonContract;

/**
 * Class PokemonIndex.
 */
class PokemonIndex implements PokemonIndexContract
{
    /**
     * @var int $count
     */
    private $count = 0;

    /**
     * @var array
     */
    private $pokemon;

    /**
     * @var Client
     */
    private $client;

    /**
     * PokemonIndex constructor.
     *
     * @param Client $client
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __construct(Client $client)
    {
        $this->client = $client;

        $response = $this->client->request('GET', 'pokemon/');

        if ($response->getStatusCode() === 200) {
            $data = json_decode($response->getBody()->getContents());
            $this->count = $data->count;
            foreach($data->results as $pokemon){
                $this->pokemon[$this->getIdFromURL($pokemon->url)] = $pokemon->name;
            }
        } else {
            throw new \RuntimeException('Failed to get data from PokeAPI.');
        }
    }

    /**
     * @param $url
     * @return mixed
     */
    private function getIdFromURL($url)
    {
        $urlArray = explode('/',substr($url,0,-1));
        return end($urlArray);
    }

    /**
     * @return array
     */
    public function index()
    {
        return $this->pokemon;
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->count;
    }

    /**
     * @return PokemonContract
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function first()
    {
        reset($this->pokemon);
        return new Pokemon(key($this->pokemon), $this->client);
    }

    /**
     * @param $name
     *
     * @return PokemonIndexContract
     */
    public function search($name)
    {
        return needleSearch($this->pokemon, $name);
    }
}
