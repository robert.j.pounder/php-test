<?php

if (!function_exists('needleSearch')) {
    function needleSearch($array, $needle)
    {
        return array_filter($array, function ($item) use ($needle) {
            return false !== stripos($item, $needle);
        });
    }
}
