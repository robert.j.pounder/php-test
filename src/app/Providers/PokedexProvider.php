<?php

namespace Pokedex\Providers;

use Illuminate\Support\ServiceProvider;
use Pokedex\Contracts\PokedexContract;
use Pokedex\Packages\Pokedex\Pokedex;

class PokedexProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind(PokedexContract::class, Pokedex::class);
    }
}
