<?php

namespace Pokedex\Contracts;

/**
 * Interface PokemonIndexContract
 * @package Pokedex\Contracts
 */
interface PokemonIndexContract
{
    /**
     * @return array
     */
    public function index();

    /**
     * @return int
     */
    public function count();

    /**
     * @return PokemonContract
     */
    public function first();

    /**
     * @param $name
     * @return PokemonIndexContract
     */
    public function search($name);

}