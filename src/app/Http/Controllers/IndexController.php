<?php

namespace Pokedex\Http\Controllers;

use Pokedex\Contracts\PokedexContract;

class IndexController extends Controller
{
    /**
     * @param PokedexContract $pokedex
     * @param int|null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(PokedexContract $pokedex, $id = null)
    {
        if($id !== null && is_numeric($id) === false){
            throw new \RuntimeException('ID can not be a string.');
        }

        try {
            $pokedexIndex = $pokedex->index();
            $count = $pokedexIndex->count();

            if (request()->has('search')) {
                $pokemonIndex = $pokedexIndex->search(request()->search);
            } else {
                $pokemonIndex = $pokedexIndex->index();
            }

            if (null !== $id && 0 !== $id) {
                $currentPokemon = $pokedex->get($id);
            } else {
                $currentPokemon = $pokedexIndex->first();
            }
        } catch (\Exception $e) {
            throw new \RuntimeException('Error with pokedex api. ['.$e->getMessage().']');
        }

        return view('index', compact('pokemonIndex', 'currentPokemon', 'count'));
    }
}
