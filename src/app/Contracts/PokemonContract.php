<?php

namespace Pokedex\Contracts;

/**
 * Interface PokemonContract
 * @package Pokedex\Contracts
 */
interface PokemonContract
{
    /**
     * @return string
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getWeight();

    /**
     * @return string
     */
    public function getHeight();

    /**
     * @return string
     */
    public function getSpecies();

    /**
     * @return string
     */
    public function getAbilities();

    /**
     * @return string
     */
    public function getSprite();
}
