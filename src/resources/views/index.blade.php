@extends('layouts.main')

@section('nav')
    <form method="get" action="/">
        <label for="search">Search: </label>
        <input id="search" type="text" value="{{request()->has('search') ? request()->all()['search'] : null}}" name="search">
        <button class="btn btn-primary">Go!</button>
    </form>
    <div class="pokemon-index">
        {{--{{var_dump($pokemon)}}--}}
        @foreach($pokemonIndex as $pokemonId => $pokemonName)
            <div class="pokemon-link">
                <a href="{{route('index',['id'=>$pokemonId])}}">{{$pokemonName}}</a> (#{{$pokemonId}})
            </div>
        @endforeach
    </div>
@endsection

@section('content')
    <div class="pokemon-index">
        <div class="pokemon-pic">
            <img src="{{$currentPokemon->getSprite()}}">
        </div>
        <div class="pokemon-stats">
            Name: #{{$currentPokemon->getId()}} {{$currentPokemon->getName()}}<br>
            Species: {{$currentPokemon->getSpecies()}}<br>
            Weight: {{$currentPokemon->getWeight()}}<br>
            Height: {{$currentPokemon->getHeight()}}<br>
            Abilities: {{$currentPokemon->getAbilities()}}<br>
        </div>
    </div>
@endsection