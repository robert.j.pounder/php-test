<?php

namespace Pokedex\Packages\Pokedex;

use GuzzleHttp\Client;
use Pokedex\Contracts\PokedexContract;
use Pokedex\Contracts\PokemonIndexContract;
use Pokedex\Contracts\PokemonContract;
use Pokedex\Packages\Pokedex\Pokemon\Pokemon;
use Pokedex\Packages\Pokedex\Pokemon\PokemonIndex;

/**
 * Class Pokedex
 * @package Pokedex\Packages\Pokedex
 */
class Pokedex implements PokedexContract
{
    /**
     * @var Client
     */
    private $client;

    /**
     * Pokedex constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://pokeapi.co/api/v2/',
        ]);
    }

    /**
     * @return PokemonIndexContract
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index()
    {
        return new PokemonIndex($this->client);
    }

    /**
     * @param $pokemonId
     *
     * @return PokemonContract
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($pokemonId)
    {
        return new Pokemon($pokemonId, $this->client);
    }
}
