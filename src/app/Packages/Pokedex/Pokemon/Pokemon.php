<?php

namespace Pokedex\Packages\Pokedex\Pokemon;

use GuzzleHttp\Client;
use Pokedex\Contracts\PokemonContract;

class Pokemon implements PokemonContract
{
    /**
     * @var int $id
     */
    private $id;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var int $weight
     */
    private $weight;

    /**
     * @var int $height
     */
    private $height;

    /**
     * @var string $species
     */
    private $species;

    /**
     * @var string $abilities
     */
    private $abilities;

    /**
     * @var string $sprite
     */
    private $sprite;

    /**
     * @var string $pokemonRawData
     */
    private $pokemonRawData;

    /**
     * @var Client $client
     */
    private $client;

    /**
     * Pokemon constructor.
     *
     * @param $pokemonId
     * @param Client $client
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __construct($pokemonId, Client $client)
    {
        $this->id = $pokemonId;
        $this->client = $client;

        $response = $this->client->request('GET', 'pokemon/'.$pokemonId);

        if ($response->getStatusCode() === 200) {
            $this->pokemonRawData = json_decode($response->getBody()->getContents());
            $this->id = $this->pokemonRawData->id;
            $this->name = $this->pokemonRawData->name;
            $this->weight = $this->pokemonRawData->weight;
            $this->height = $this->pokemonRawData->height;
            $this->species = $this->pokemonRawData->species->name;
            $this->sprite = $this->pokemonRawData->sprites->front_default;

            $abilities = '';
            foreach($this->pokemonRawData->abilities as $ability){
                $abilities .= $ability->ability->name.', ';
            }
            $this->abilities = substr($abilities,0,-2).'.';
        } else {
            throw new \RuntimeException('Failed to get data from PokeAPI.');
        }
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return string
     */
    public function getSpecies()
    {
        return $this->species;
    }

    /**
     * @return string
     */
    public function getAbilities()
    {
        return $this->abilities;
    }

    /**
     * @return string
     */
    public function getSprite()
    {
        return $this->sprite;
    }
}
