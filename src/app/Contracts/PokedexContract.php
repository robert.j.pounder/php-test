<?php

namespace Pokedex\Contracts;

/**
 * Interface PokedexContract
 * @package Pokedex\Contracts
 */
interface PokedexContract
{
    /**
     * @return PokemonIndexContract
     */
    public function index();

    /**
     * @param $pokemonId
     * @return PokemonContract
     */
    public function get($pokemonId);
}