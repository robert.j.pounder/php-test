<!DOCTYPE html>
<html lang="en-GB">
<head>
    <title>Pokedex</title>
    <script src="{{asset('js/app.js')}}"></script>
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-6">
                @yield('nav')
            </div>
            <div class="col-6">
                @yield('content')
            </div>
        </div>
    </div>
</body>
</html>